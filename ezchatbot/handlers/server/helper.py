import time
import uuid

from ezchatbot.chatbots.messages import Message
from ezchatbot.handlers.server.types import ChatCompletion, ChatCompletionChunk, Usage


class ReponseHelper:
    @staticmethod
    def generate_usage_estimation(messages: list[Message], choices: list[str]) -> Usage:
        """
        Generate usage estimation for the given messages and choices (1 work = 1 token, in reality it's more).
        """
        # TODO: use a real token number
        prompt_tokens = sum(
            len(message.get("content", "").strip().split(" "))
            for message in messages
            if message.get("role") == "user"
        )
        completion_tokens = sum(len(choice) for choice in choices) + sum(
            len(message.get("content", "").strip().split(" "))
            for message in messages
            if message.get("role") == "assistant"
        )

        return {
            "prompt_tokens": prompt_tokens,
            "completion_tokens": completion_tokens,
            "total_tokens": prompt_tokens + completion_tokens,
        }

    @staticmethod
    def create_completion_from_strs(
        messages: list[Message], choices: list[str]
    ) -> ChatCompletion:
        """
        Create a completion from the given messages and choices.
        """
        return {
            "id": str(uuid.uuid4()),
            "object": "chat.completion",
            "created": int(time.time()),
            "choices": [
                {
                    "message": {"content": choice, "role": "assistant"},
                    "finish_reason": "stop",
                    "index": i,
                    "logprobs": None,
                }
                for i, choice in enumerate(choices)
            ],
            "system_fingerprint": "ezchatbot",
            "model": "ezchatbot",
            "usage": ReponseHelper.generate_usage_estimation(messages, choices),
        }

    @staticmethod
    def create_full_chunk_from_strs(choices: list[str]) -> ChatCompletionChunk:
        """
        Create a full chunk from the given choices.
        """
        return {
            "id": str(uuid.uuid4()),
            "created": int(time.time()),
            "choices": [
                {
                    "delta": {"content": choice, "role": "assistant"},
                    "finish_reason": "stop",
                    "index": i,
                    "logprobs": None,
                }
                for i, choice in enumerate(choices)
            ],
            "system_fingerprint": "ezchatbot",
            "model": "ezchatbot",
            "object": "chat.completion.chunk",
        }
