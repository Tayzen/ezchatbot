from typing import Literal, NotRequired, Optional, TypedDict

from ezchatbot.chatbots.messages import Message


class TopLogprobs(TypedDict):
    """
    The best logprobs.
    """

    token: int
    logprob: float
    bytes: list[int]


class LogprobsContent(TypedDict):
    """
    The content of the logprobs.
    """

    token: int
    logprob: float
    bytes: list[int]
    top_logprobs: list[TopLogprobs]


class Logprobs(TypedDict):
    """
    Logprobs returned by the API.
    """

    content: Optional[list[LogprobsContent]]


class Choice(TypedDict):
    """
    Choice returned by the API.
    Note: The project doesn't return the tools as it executes it itself, so the dedicated values are never returned.
    """

    message: Message
    finish_reason: Literal["length", "stop", "content_filter"]
    index: int
    logprobs: Optional[Logprobs]


class Usage(TypedDict):
    """
    API usage to generate this message.
    """

    prompt_tokens: int
    completion_tokens: int
    total_tokens: int


class ChatCompletion(TypedDict):
    """
    The chat completion returned by the API.
    """

    id: str
    created: int
    choices: list[Choice]
    system_fingerprint: str
    usage: Usage
    model: str
    object: Literal["chat.completion"]


class Delta(TypedDict):
    """
    A chunk of a message.
    """

    content: str
    role: Literal["system", "user", "assistant"]


class ChunkChoice(TypedDict):
    """
    Chunk of the choices.
    """

    delta: Delta
    finish_reason: Optional[Literal["length", "stop", "content_filter"]]
    index: int
    logprobs: Optional[Logprobs]


class ChatCompletionChunk(TypedDict):
    """
    The chat completion chunk returned by the API.
    """

    id: str
    created: int
    choices: list[ChunkChoice]
    system_fingerprint: str
    model: str
    usage: NotRequired[Usage]
    service_tier: NotRequired[Optional[str]]
    object: Literal["chat.completion.chunk"]
