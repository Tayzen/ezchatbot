from dataclasses import dataclass
from typing import Union

import fastapi

from ezchatbot.chatbots.interface import IChatbot
from ezchatbot.chatbots.messages import Message
from ezchatbot.handlers.server.helper import ReponseHelper
from ezchatbot.handlers.server.types import ChatCompletion, ChatCompletionChunk


@dataclass
class ServerHandler:
    """
    A handler for serving a chatbot (compatible with the openAI chat completion endpoint).
    """

    chatbot: IChatbot
    app = fastapi.FastAPI()

    def serve(self):
        """
        Setup and launch a server for the chatbot.
        You must store the returned app in an object (or get the app attribute).
        """
        app = self.app

        @app.get("/")
        async def root():
            """
            The root endpoint which can be used to check if the server is running.

            Returns:
                dict: The response
            """
            # TODO: use a real response
            return {"message": "Server is running"}

        @app.post("/v1/chat/completions")
        async def chat(  # TODO: kwargs?
            messages: list[Message],
            stream: bool = False,
        ) -> Union[ChatCompletion, ChatCompletionChunk]:
            """
            The chatting endpoint.

            Args:
                messages (list[Message]): The messages.
                n (int, optional): The number of responses. Defaults to 1.
                stream (bool, optional): Whether to stream the responses. Defaults to False.

            Returns:
                Union[ChatCompletion, ChatCompletionChunk]: The chat completion
            """
            # TODO: handle a real stream response and multiple choices
            query_response = self.chatbot.execute_query(
                messages[-1].get("content", "No query provided"),  # TODO: error
                previous_messages=messages[:-1],
            )
            match query_response:
                case list(results):
                    str_response = "\n".join(str(result) for result in results)
                case str(response):
                    str_response = response

            if stream:
                return ReponseHelper.create_full_chunk_from_strs([str_response])
            return ReponseHelper.create_completion_from_strs(messages, [str_response])

        @app.post("/v1/query")
        async def query(query: str) -> str:
            """
            An endpoint to query the chatbot. It's easier to use than /v1/chat/completions.

            Args:
                query (str): The query to execute

            Returns:
                str: The response
            """
            match self.chatbot.execute_query(query):
                case list(results):
                    return "\n".join(str(result) for result in results)
                case str(response):
                    return response
                case _:
                    return "No response"

        return app
