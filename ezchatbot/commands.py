"""
The commands module, used to parse and execute functions.
"""

import json
from dataclasses import dataclass
from inspect import Parameter, signature
from typing import Callable, Dict, Optional, TypedDict

from pydantic import BaseModel, create_model


class MissingCommandParametersException(Exception):
    """
    Exception raised when a command is called with missing parameters.
    """


class UnavailableCommandException(Exception):
    """
    Exception raised when a command is called but it is not available.
    """


@dataclass
class InputCommand:
    """
    A dataclass representing an input for a command.
    """

    name: str
    input_type: type
    default_value: Optional[object]

    @classmethod
    def parse_parameter(cls, param: Parameter) -> "InputCommand":
        """
        A class method that parses a parameter and returns an InputCommand object.

        Args:
            cls: The class itself.
            param: The parameter to be parsed (of type Parameter).

        Returns:
            An InputCommand object.
        """
        name = param.name
        input_type = param.annotation
        if param.default is not Parameter.empty:
            default_value = param.default
        else:
            default_value = None

        return cls(name, input_type, default_value)

    def is_optional(self) -> bool:
        """
        Check if the input is optional.

        Returns:
            bool: True if the input is optional, False otherwise.
        """
        return self.default_value is not None


@dataclass
class Command:
    """
    A dataclass representing a command with its name, description, list of input and return type.
    """

    name: str
    description: str
    inputs: dict[str, InputCommand]
    returns: type
    fun: Callable

    def __post_init__(self):
        self._model = None

    @classmethod
    def parse_function(cls, func: Callable) -> "Command":
        """
        A method to parse a given function into a Command object, extracting its name, function,
        description, inputs, and return types.

        Args:
            cls: The class itself.
            func: The function to be parsed.

        Returns:
            A Command object.
        """
        sign = signature(func)
        new_cmd = Command(
            func.__name__,
            (func.__doc__ or "").strip(),
            {},
            sign.return_annotation,
            func,
        )

        for _, parameter in sign.parameters.items():
            if not parameter.kind in [Parameter.VAR_KEYWORD, Parameter.VAR_POSITIONAL]:
                new_cmd.inputs[parameter.name] = InputCommand.parse_parameter(parameter)

        return new_cmd

    def get_kwargs(self, *args, **kwargs) -> dict:
        """Reconstruct kwargs with args and default parameters"""

        for arg, parameter in zip(args, self.inputs.values()):
            # should work because dict are ordered
            kwargs[parameter.name] = arg or parameter.default_value
        for key, value in kwargs.items():
            if (value is None) or not value:
                kwargs[key] = self.inputs[key].default_value
        kwargs = {
            **{
                param.name: param.default_value
                for param in self.inputs.values()
                if param.default_value is not None
            },
            **kwargs,
        }

        inputs_names = list(self.inputs.keys())

        # filtering unwanted arguments
        kwargs = {key: value for key, value in kwargs.items() if key in inputs_names}

        # verify inputs validity
        for name in inputs_names:
            if not name in kwargs.keys():
                raise MissingCommandParametersException(f"Missing parameter {name}")
        self.params_model.model_validate(kwargs)

        return kwargs

    def execute(self, *args, **kwargs) -> object:
        """Execute the fun function arguments

        Returns:
            Object: An object of type returns
        """
        return self.fun(**self.get_kwargs(*args, **kwargs))

    def is_valid(self, *args, **kwargs) -> bool:
        """Check if the command is valid

        Returns:
            bool: True if the command is valid, False otherwise
        """
        try:
            self.get_kwargs(*args, **kwargs)
            return True
        except Exception:  # pylint: disable=broad-except
            return False

    @property
    def params_model(self) -> BaseModel:  # TODO: use the model only for the params
        """
        Return the model of the parameters for the command.

        Returns:
            BaseModel: The model of the parameters
        """
        if not self._model:
            self._model = create_model(
                self.name,
                **{  # type: ignore
                    inp.name: (inp.input_type, inp.default_value)
                    for inp in self.inputs.values()
                },
            )
        return self._model

    def schema(self) -> dict:
        """
        Return a representation of the object in JSON schema format.

        Returns:
            dict: A representation of the command in JSON schema format.
        """

        def remove_titles(schema):
            if "title" in schema:
                del schema["title"]
            if "properties" in schema:
                for prop in schema["properties"].values():
                    remove_titles(prop)
            return schema

        def remove_defaults_if_null(schema):
            if "default" in schema and schema["default"] is None:
                del schema["default"]
            if "properties" in schema:
                for prop in schema["properties"].values():
                    remove_defaults_if_null(prop)
            return schema

        def replace_refs(schema):
            # TODO: check if it works
            if "$ref" in schema:
                schema["$ref"] = schema["$ref"].replace(
                    "#/$defs/", "#/function/parameters/$defs/"
                )
            if "properties" in schema:
                for prop in schema["properties"].values():
                    replace_refs(prop)
            return schema

        return {
            "type": "function",
            "function": {
                "name": self.name,
                "strict": True,
                "description": self.description,
                "parameters": {
                    **replace_refs(
                        remove_defaults_if_null(
                            remove_titles(
                                self.params_model.model_json_schema(
                                    mode="serialization"
                                )
                            )
                        )
                    ),
                    **{
                        "required": [
                            param.name
                            for param in self.inputs.values()
                            if not param.is_optional()
                        ],
                        "additionalProperties": False,
                    },
                },
            },
        }

    def to_json_defintion(self) -> str:
        """
        Return a string representation of the object in JSON schema format.

        Returns:
            str: A string representation of the object in JSON schema format.
        """
        return json.dumps(self.schema())


class CommandParameters(TypedDict):  # TODO: rename FunctionCallingOutput?
    """
    A dictionary representing the parameters of a command.
    """

    name: str
    parameters: dict[str, object]


@dataclass
class CommandsContainer:
    """
    An handler for a list of commands, with methods to add and execute them.
    """

    commands: Dict[str, Command]

    def add_command(self, fun: Callable) -> Command:
        """
        A method to parse a given function and create a Command object with its name, function,
        description, inputs, and return type.

        Args:
            cls: The class object.
            func (Callable): The function to be parsed.

        Returns:
            Command: The Command object created from the parsed function.
        """
        new_cmd = Command.parse_function(fun)
        self.commands[new_cmd.name] = new_cmd
        return new_cmd

    def _get_unavailable(self, commands: list[CommandParameters]) -> list[str]:
        """Check if all the commands are available

        Args:
            commands (list[CommandParameters]): The list of commands to check.

        Returns:
            list[str]: The list of unavailable commands.
        """
        unavailable_commands = [
            command["name"]
            for command in commands
            if "name" in command.keys() and command["name"] not in self.commands
        ]
        return unavailable_commands

    def is_valid(self, commands: list[CommandParameters]) -> bool:
        """Check if all the commands are valid

        Args:
            commands (list[CommandParameters]): The list of commands to check.

        Returns:
            bool: True if all the commands are valid, False otherwise.
        """
        if self._get_unavailable(commands):
            return False

        for command in commands:
            if "name" not in command:
                return False
            if not self.commands[command["name"]].is_valid(
                **command.get("parameters", {})
            ):
                return False
        return True

    def execute(self, commands: list[CommandParameters]) -> list[object]:
        """Execute the fun function arguments

        Returns:
            Object: An object of type returns
        """
        # TODO: raise errors only when all the commands are wrong, otherwise return the result from the right commands
        if self.is_valid(commands):
            return [
                self.commands[command["name"]].execute(**command.get("parameters", {}))
                for command in commands
            ]
        raise UnavailableCommandException(  # TODO: return correct exception
            f"Some commands are not valid: {commands}"
        )

    @classmethod
    def empty(cls) -> "CommandsContainer":
        """
        A class method to create an empty CommandsContainer object.

        Returns:
            CommandsContainer: An instance of CommandsContainer with an empty commands dictionary.
        """
        return cls(commands={})

    def to_json_defintion(self) -> str:
        """
        Return a string representation of the object in JSON schema format.

        Returns:
            str: A string representation of the object in JSON schema format.
        """
        return "\n".join(
            [command.to_json_defintion() for command in self.commands.values()]
        )

    def format_commands(self) -> str:
        """
        Return a string representation of the object in JSON format.

        Returns:
            str: A string representation of the object in JSON format.
        """
        return self.to_json_defintion()
