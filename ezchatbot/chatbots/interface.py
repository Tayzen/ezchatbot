from abc import ABC, abstractmethod
from typing import Callable, Optional, Union

from ezchatbot.chatbots.messages import Message, NoResponse
from ezchatbot.commands import CommandParameters, CommandsContainer


class IChatbot(ABC):
    """
    Interface for chatbots.
    """

    _commands: CommandsContainer

    @classmethod
    @abstractmethod
    def from_config(cls, path: str) -> "IChatbot":
        """
        Create a chatbot from a config file

        Args:
            path (str): The path to the config file

        Returns:
            Chatbot: The created chatbot
        """

    @abstractmethod
    def execute_function_calling_model(
        self, query: str, previous_messages: Optional[list[Message]] = None
    ) -> Union[list[CommandParameters], NoResponse]:
        """
        Execute a query (user input) with the function calling model

        Args:
            query (str): The query to execute
            previous_messages (list[Message]): The previous messages

        Returns:
            Union[list[CommandParameters], NoResponse]: The response
        """

    @abstractmethod
    def execute_backup_model(
        self, query: str, previous_messages: Optional[list[Message]] = None
    ) -> str:
        """
        Execute a query (user input)

        Args:
            query (str): The query to execute
            previous_messages (list[Message]): The previous messages

        Returns:
            str: The response
        """

    @abstractmethod
    def command(self) -> Callable[[Callable], None]:
        """
        A decorator to define a command in the chatbot
        """

    @abstractmethod
    def execute_query(
        self, query: str, previous_messages: Optional[list[Message]] = None
    ) -> Union[list[object], str]:  # TODO: list[str]?
        """
        Execute a query (user input)

        Args:
            query (str): The query to execute
            previous_messages (list[Message]): The previous messages

        Returns:
            Union[str, list[object]]: The response
        """
