class ModelExecutionError(Exception):
    """
    An exception generated when the model can't perform its operation.
    """


class NoModelDefinedError(Exception):
    """
    An exception generated when an execution is called without a defined model.
    """


class NoQueryError(Exception):
    """
    An exception generated when an execution is called without a query.
    """


class MissingSystemPromptError(Exception):
    """
    An exception generated when an execution is called without a system prompt.
    """
