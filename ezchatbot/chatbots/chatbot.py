import json
import logging
from abc import abstractmethod
from dataclasses import dataclass, field
from datetime import datetime
from typing import Callable, Optional, Union

from pydantic import ValidationError

from ezchatbot.chatbots.dialog import Dialog
from ezchatbot.chatbots.exceptions import (MissingSystemPromptError,
                                           NoModelDefinedError, NoQueryError)
from ezchatbot.chatbots.interface import IChatbot
from ezchatbot.chatbots.messages import NO_RESPONSE, Message, NoResponse
from ezchatbot.commands import (CommandParameters, CommandsContainer,
                                MissingCommandParametersException,
                                UnavailableCommandException)


@dataclass(kw_only=True)
class Chatbot(IChatbot):
    """
    An abstract chatbot with most features already implemented.
    """

    function_callling_model: str
    function_calling_system_prompt: str
    backup_model: Optional[str]
    backup_system_prompt: Optional[str]
    metadata: dict[str, object] = field(default_factory=dict)
    _commands: CommandsContainer = field(default_factory=CommandsContainer.empty)

    def __post_init__(self):
        if not self.function_callling_model:
            raise NoModelDefinedError("No function calling model defined")
        if not self.backup_system_prompt and self.backup_model:
            raise MissingSystemPromptError(
                "A backup system prompt is required if a backup model is defined"
            )
        self._function_calling_system_prompt: str = ""
        self._backup_system_prompt: str = ""

    def _update_function_callling_prompt(self):
        now = datetime.now()
        self._function_calling_system_prompt = self.function_calling_system_prompt.format(
            **{
                "commands": self._commands.format_commands(),
                "day_of_week": now.strftime("%A"),
                "date": now.strftime("%Y-%m-%d"),
                "time": now.strftime("%H:%M:%S"),
                # metadata should override other parameters (useful for debugging and benchmarking)
                **self.metadata,
            },
        )

    def _update_backup_prompt(self):
        now = datetime.now()
        if self.backup_model and self.backup_system_prompt:
            self._backup_system_prompt = self.backup_system_prompt.format(
                **{
                    "commands": self._commands.format_commands(),
                    "day_of_week": now.strftime("%A"),
                    "date":now.strftime("%Y-%m-%d"),
                    "time":now.strftime("%H:%M:%S"),
                    # metadata should override other parameters (useful for debugging and benchmarking)
                    **self.metadata,
                },
            )

    def _update_prompts(self):
        self._update_function_callling_prompt()
        if self.backup_model:
            self._update_backup_prompt()

    @classmethod
    def from_config(cls, path: str) -> "Chatbot":
        """
        Create a chatbot from a config file

        Args:
            path (str): The path to the config file

        Returns:
            Chatbot: The created chatbot
        """
        with open(path, "r", encoding="utf-8") as fp:
            config = json.load(fp)
        return cls(**config)

    def command(self):  # TODO: metaparameters (use args and kwargs)?
        """
        A decorator to define a command in the chatbot
        """
        # TODO: rename tool?
        # TODO: metaparameters for returning the query as first parameter
        # TODO: mataparameter to reformulate the response (can be done at chatbot level too)

        def inner(func: Callable):
            self._commands.add_command(func)
            self._update_prompts()

        return inner

    @abstractmethod
    def execute_backup_model(
        self, query: str, previous_messages: Optional[list[Message]] = None
    ) -> str:
        """
        Execute a query (user input)

        Args:
            query (str): The query to execute
            previous_messages (list[Message]): The previous messages

        Returns:
            str: The response
        """

    @abstractmethod
    def execute_function_calling_model(
        self, query: str, previous_messages: Optional[list[Message]] = None
    ) -> Union[list[CommandParameters], NoResponse]:
        """
        Execute a query (user input) with the function calling model

        Args:
            query (str): The query to execute
            previous_messages (list[Message]): The previous messages

        Returns:
            Union[list[CommandParameters], NoResponse]: The response
        """

    def execute_query(
        self, query: str, previous_messages: Optional[list[Message]] = None
    ) -> Union[list[object], str]:  # TODO: list[str]?
        """
        Execute a query (user input)

        Args:
            query (str): The query to execute
            previous_messages (list[Message]): The previous messages

        Returns:
            Union[str, list[object]]: The response
        """
        if not query:
            raise NoQueryError("No query provided")

        self._update_function_callling_prompt()

        match self.execute_function_calling_model(query, previous_messages):
            case list(commands) if len(commands) > 0:
                try:
                    result = self._commands.execute(commands)
                    if result != NO_RESPONSE:
                        return result
                except UnavailableCommandException as e:
                    logging.warning(e)
                except MissingCommandParametersException as e:
                    logging.warning(e)
                except ValidationError as e:
                    logging.warning(e)

        self._update_backup_prompt()
        return self.execute_backup_model(query, previous_messages)

    def start_dialog(self):
        """
        Start a new dialog
        """
        return Dialog(chatbot=self)
