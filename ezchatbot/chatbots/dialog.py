from dataclasses import dataclass, field

from ezchatbot.chatbots.interface import IChatbot
from ezchatbot.chatbots.messages import Message, Roles


@dataclass
class Dialog:
    """
    A dialog between the user and the assistant
    """

    chatbot: IChatbot
    history: list[Message] = field(default_factory=list)

    def speak(self, query: str) -> str:
        """
        Add a user query to the history and return the response

        Args:
            query (str): The query to execute

        Returns:
            str: The response
        """
        self.history.append(Message(role=Roles.USER, content=query))
        match self.chatbot.execute_query(query, self.history):
            case str(response):
                response = response.strip()
            case list(commands):
                response = "\n".join(str(command) for command in commands)
        self.history.append(Message(role=Roles.ASSISTANT, content=response))
        return response
