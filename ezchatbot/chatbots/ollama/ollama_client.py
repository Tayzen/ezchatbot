import logging
from dataclasses import dataclass
from typing import NotRequired, Optional, TypedDict

import ollama

from ezchatbot.chatbots.messages import Message, Roles


class OllamaResponse(TypedDict):
    """
    The response from the ollama model
    """

    data: NotRequired[str]
    error: NotRequired[str]


class OllamaParams(TypedDict):
    """
    The parameters for running an ollama model
    """

    model: str
    system_prompt: str
    function_calling_model: NotRequired[bool]
    temperature: NotRequired[float]
    top_p: NotRequired[float]
    top_k: NotRequired[int]
    num_ctx: NotRequired[int]


def _convert_message_to_ollama(message: Message) -> ollama.Message:
    return {"role": message["role"], "content": message["content"]}


@dataclass
class OllamaClient:
    """
    An ollama client wrapper
    """

    url: str
    port: int

    def __post_init__(self):
        self.client = ollama.Client(f"http://{self.url}:{self.port}")

    def _pull_model(self, model_name: str) -> None:
        if model_name not in self._get_available_models():
            logging.info(f"Pulling model: {model_name} from ollama")
            self.client.pull(model_name)

    def _get_available_models(self) -> list[str]:
        return [model.get("model") for model in self.client.list().get("models", [])]

    def _composed_messages(
        self, query: str, system_prompt: str, previous_messages: list[Message]
    ) -> list[ollama.Message]:
        messages = []
        if not (previous_messages and previous_messages[0].get("role") == Roles.SYSTEM):
            messages.append({"role": Roles.SYSTEM, "content": system_prompt})
        messages.extend(map(_convert_message_to_ollama, previous_messages))
        messages.append({"role": Roles.USER, "content": query})
        return messages

    def generate(
        self,
        query: str,
        model_params: OllamaParams,
        previous_messages: Optional[list[Message]] = None,
    ) -> OllamaResponse:
        """
        Generate a response using ollama with ModelParameters
        """
        # TODO: add an option "keep_alive"
        self._pull_model(model_params["model"])
        logging.debug(
            f"Generating response using model: {model_params} and query: {query}"
        )
        if previous_messages is None:
            previous_messages = []

        response = self.client.chat(
            model=model_params["model"],
            messages=self._composed_messages(
                query, model_params.get("system_prompt", ""), previous_messages
            ),
            format="json" if model_params.get("function_calling_model", False) else "",
            options={
                "temperature": model_params.get("temperature"),
                "top_p": model_params.get("top_p"),
                "top_k": model_params.get("top_k"),
                "num_ctx": model_params.get("num_ctx"),
            },  # TODO: what to do if the value is None?
        )
        match response:
            case {"message": {"content": str(content)}}:
                return {"data": content}
        return {"error": "Unexpected response from ollama"}
