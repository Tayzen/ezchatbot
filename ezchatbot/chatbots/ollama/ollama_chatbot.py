"""
OllamaChatbot, build on top of the Chatbot class. It uses Ollama to answer user queries.
You can host an instance yourself.
"""

import json
import logging
from dataclasses import dataclass, field
from typing import Optional, Union

from typing_extensions import override

from ezchatbot.chatbots.chatbot import Chatbot
from ezchatbot.chatbots.exceptions import NoModelDefinedError
from ezchatbot.chatbots.messages import NO_RESPONSE, Message, NoResponse
from ezchatbot.chatbots.ollama.ollama_client import OllamaClient, OllamaParams
from ezchatbot.commands import CommandParameters, CommandsContainer
from ezchatbot.prompts import (DEFAULT_BACKUP_SYSTEM_PROMPT_TEMPLATE,
                               DEFAULT_FUNCTION_CALLING_SYSTEM_PROMPT_TEMPLATE)


@dataclass(kw_only=True)
class OllamaChatbot(Chatbot):
    """
    A Chatbot based on Ollama, you can handle the requests with a server you own yourself. That's freedom!
    """

    url: str
    port: int
    function_callling_model: str = "llama3.1:8b"
    function_calling_system_prompt: str = (
        DEFAULT_FUNCTION_CALLING_SYSTEM_PROMPT_TEMPLATE
    )
    backup_model: Optional[str] = "llama3.1:8b"
    backup_system_prompt: Optional[str] = DEFAULT_BACKUP_SYSTEM_PROMPT_TEMPLATE
    backup_sentence: Optional[str] = None
    metadata: dict[str, object] = field(default_factory=dict)
    function_calling_temperature: Optional[float] = None
    function_calling_top_p: Optional[float] = None
    function_calling_top_k: Optional[int] = None
    function_calling_num_ctx: Optional[int] = None
    backup_temperature: Optional[float] = None
    backup_top_p: Optional[float] = None
    backup_top_k: Optional[int] = None
    backup_num_ctx: Optional[int] = None
    _commands: CommandsContainer = field(default_factory=CommandsContainer.empty)

    def __post_init__(self) -> None:
        super().__post_init__()
        if self.backup_model and self.backup_sentence:
            raise ValueError(
                "Only one of backup_model or backup_sentence must be set (or neither of them)"
            )
        self._update_prompts()
        self._client = OllamaClient(self.url, self.port)
        # TODO: call _pull_model here (make it not private)

    def _get_function_calling_params(self) -> OllamaParams:
        return {
            "model": self.function_callling_model,
            "system_prompt": self._function_calling_system_prompt,
            "function_calling_model": True,
            **{
                k: v
                for k, v in {
                    "temperature": self.function_calling_temperature,
                    "top_p": self.function_calling_top_p,
                    "top_k": self.function_calling_top_k,
                    "num_ctx": self.function_calling_num_ctx,
                }.items()
                if v is not None
            },
        }  # type: ignore
        # ignoring type because the linter cannot understand the comprehension

    def _get_backup_params(self) -> OllamaParams:
        if self.backup_model:
            return {
                "model": self.backup_model,
                "system_prompt": self._backup_system_prompt,
                **{
                    k: v
                    for k, v in {
                        "temperature": self.backup_temperature,
                        "top_p": self.backup_top_p,
                        "top_k": self.backup_top_k,
                        "num_ctx": self.backup_num_ctx,
                    }.items()
                    if v is not None
                },
            }  # type: ignore
            # same reason to ignore type as above
        raise NoModelDefinedError("No backup model")

    def default_answer(self) -> str:
        """
        Return the default answer if no command is found
        """
        return self.backup_sentence or "I'm unable to answer your question."

    @override
    def execute_backup_model(
        self, query: str, previous_messages: Optional[list[Message]] = None
    ) -> str:
        if not previous_messages:
            previous_messages = []
        if self.backup_model:
            return self._client.generate(
                query, self._get_backup_params(), previous_messages
            ).get("data", self.default_answer())
        return self.default_answer()

    @override
    def execute_function_calling_model(
        self, query: str, previous_messages: Optional[list[Message]] = None
    ) -> Union[list[CommandParameters], NoResponse]:
        if not previous_messages:
            previous_messages = []

        function_calling_response = self._client.generate(
            query, self._get_function_calling_params(), previous_messages
        )

        match function_calling_response:
            case {"data": data}:
                logging.debug(f"Executing commands: {data}")
                try:
                    match json.loads(data):
                        case {"commands": list(commands)} if len(commands) > 0:
                            if all("name" in command for command in commands) and self._commands.is_valid(commands):
                                return commands
                except json.JSONDecodeError:
                    return NO_RESPONSE
        return NO_RESPONSE
