from dataclasses import dataclass
from typing import Literal, Protocol, TypedDict


@dataclass
class NoResponse:
    """
    A class which does nothing, it only indicates when there is no function call returned by a
    model
    """


NO_RESPONSE = NoResponse()


class Roles(Protocol):
    """
    The roles of a message (it follows the OpenAI's API).
    """

    SYSTEM: Literal["system"] = "system"
    USER: Literal["user"] = "user"
    ASSISTANT: Literal["assistant"] = "assistant"


class Message(TypedDict):
    """
    The definition of a message (it follows the OpenAI's API). You may need to convert
    the values for other APIs but it's almost a standard.
    """

    role: Literal["system", "user", "assistant"]
    content: str
