"""
Matchers for validating responses
"""

import re
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Generic, Iterable, NotRequired, Optional, TypedDict, TypeVar

from ezchatbot.commands import CommandParameters, CommandsContainer

T = TypeVar("T")


class MatcherParams(TypedDict):
    """
    The additional parameters used to configure the matchers
    """

    case_sensitive: NotRequired[bool]
    model_name: NotRequired[str]
    system_prompt: NotRequired[str]
    temperature: NotRequired[float]
    max_tokens: NotRequired[int]


class ResponseMatcher(ABC, Generic[T]):
    """
    Class for validating the response from a model
    """

    @abstractmethod
    def match(self, response: T) -> bool:
        """
        Does the response match the specific format requested by the matcher?
        """

    @abstractmethod
    def load_params(self, params: MatcherParams):
        """
        Load the parameters for the matcher

        Args:
            params (Union[OllamaMatcherParams, StringMatcherParams]): The parameters for the matcher
        """


@dataclass
class ExactResponseMatcher(ResponseMatcher):
    """
    Matches the exact response
    """

    expectation: str
    case_sensitive: bool = False

    def match(self, response: str) -> bool:
        if not isinstance(response, str):
            return False
        if not self.case_sensitive:
            return response.strip().lower() == self.expectation.lower()
        return response.strip() == self.expectation

    def load_params(self, params: MatcherParams):
        self.case_sensitive = params.get("case_sensitive", False)


@dataclass
class ContainsResponsesMatcher(ResponseMatcher):
    """
    Matches if the response contains the sub-response
    """

    sub_responses: list[str]
    case_sensitive: bool = False

    def match(self, response: str) -> bool:
        if not isinstance(response, str):
            return False
        if not self.case_sensitive:
            return any(
                sub_response.lower() in response.lower()
                for sub_response in self.sub_responses
            )
        return any(sub_response in response for sub_response in self.sub_responses)

    def load_params(self, params: MatcherParams):
        self.case_sensitive = params.get("case_sensitive", False)


@dataclass
class ContainsAllResponsesMatcher(ResponseMatcher):
    """
    Matches if the response contains the sub-response
    """

    sub_responses: list[str]
    case_sensitive: bool = False

    def match(self, response: str) -> bool:
        if not isinstance(response, str):
            return False
        if not self.case_sensitive:
            return all(
                sub_response.lower() in response.lower()
                for sub_response in self.sub_responses
            )
        return all(sub_response in response for sub_response in self.sub_responses)

    def load_params(self, params: MatcherParams):
        self.case_sensitive = params.get("case_sensitive", False)


@dataclass
class StartsWithResponseMatcher(ResponseMatcher):
    """
    Matches if the response starts with the start_response
    """

    start_response: str
    case_sensitive: bool = False

    def match(self, response: str) -> bool:
        if not isinstance(response, str):
            return False
        if not self.case_sensitive:
            return response.strip().lower().startswith(self.start_response.lower())
        return response.strip().startswith(self.start_response)

    def load_params(self, params: MatcherParams):
        self.case_sensitive = params.get("case_sensitive", False)


@dataclass
class EndsWithResponseMatcher(ResponseMatcher):
    """
    Matches if the response ends with the end_response
    """

    end_response: str
    case_sensitive: bool = False

    def match(self, response: str) -> bool:
        if not isinstance(response, str):
            return False
        if not self.case_sensitive:
            return response.strip().lower().endswith(self.end_response.lower())
        return response.strip().endswith(self.end_response)

    def load_params(self, params: MatcherParams):
        self.case_sensitive = params.get("case_sensitive", False)


@dataclass
class NotContainsResponseMatcher(ResponseMatcher):
    """
    Matches if the response does not contain the not_expected
    """

    not_expected: list[str]
    case_sensitive: bool = False

    def match(self, response: str) -> bool:
        if not isinstance(response, str):
            return False
        if not self.case_sensitive:
            return not any(
                item for item in self.not_expected if item.lower() in response.lower()
            )
        return not any(item for item in self.not_expected if item in response)

    def load_params(self, params: MatcherParams):
        self.case_sensitive = params.get("case_sensitive", False)


@dataclass
class RegexResponseMatcher(ResponseMatcher):
    """
    Matches if the response matches the regex
    """

    regex: str

    def match(self, response: str) -> bool:
        if not isinstance(response, str):
            return False
        return re.match(self.regex, response.strip()) is not None

    def load_params(self, _: MatcherParams):  # type: ignore[reportUnusedVariable]
        # No additional parameters
        pass


@dataclass
class FunctionCallingMatcher(ResponseMatcher):
    """
    Matches if the response matches the expected functions.
    """

    expectation: Iterable[CommandParameters]
    _commands_container: Optional[CommandsContainer] = None

    def add_commands_container(self, commands_container: CommandsContainer):
        """
        Add the commands container to the matcher. This is used to take into account the default parameters.

        Args:
            commands_container (CommandsContainer): The commands container to add.
        """
        self._commands_container = commands_container

    def _compare(self, response: CommandParameters) -> bool:
        function_name = response["name"]
        parameters = response["parameters"]
        if (
            self._commands_container is not None
            and function_name in self._commands_container.commands.keys()
        ):
            try:
                parameters = self._commands_container.commands[
                    function_name
                ].get_kwargs(**parameters)
            except Exception:  # pylint: disable=broad-except
                return False

        expected_functions = [command["name"] for command in self.expectation]
        if function_name not in expected_functions:
            return False

        return any(
            all(
                parameters.get(param_name) == param_value
                for param_name, param_value in command["parameters"].items()
            )
            for command in self.expectation
            if command["name"] == function_name
        )

    def match(self, response: list[CommandParameters]) -> bool:
        if not isinstance(response, list):
            return False

        return all(self._compare(command) for command in response)

    def load_params(self, _: MatcherParams):  # type: ignore[reportUnusedVariable]
        # No additional parameters
        pass


# TODO: LLM matchers language + meaning


@dataclass
class OrResponseMatcher(ResponseMatcher):
    """
    Matches if any of the matchers match
    """

    matchers: list[ResponseMatcher]

    def match(self, response: str) -> bool:
        return any(matcher.match(response) for matcher in self.matchers)

    def load_params(self, _: MatcherParams):  # type: ignore[reportUnusedVariable]
        # No additional parameters
        pass


@dataclass
class AndResponseMatcher(ResponseMatcher):
    """
    Matches if all of the matchers match
    """

    matchers: list[ResponseMatcher]

    def match(self, response: str) -> bool:
        return all(matcher.match(response) for matcher in self.matchers)

    def load_params(self, _: MatcherParams):  # type: ignore[reportUnusedVariable]
        # No additional parameters
        pass


@dataclass
class NotResponseMatcher(ResponseMatcher):
    """
    Matches if none of the matcher are matching
    """

    matchers: list[ResponseMatcher]

    def match(self, response: str) -> bool:
        return not any(matcher.match(response) for matcher in self.matchers)

    def load_params(self, _: MatcherParams):  # type: ignore[reportUnusedVariable]
        # No additional parameters
        pass
