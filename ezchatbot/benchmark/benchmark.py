import itertools
from dataclasses import dataclass
from typing import Callable, Iterable, Optional, Type, Unpack

from ezchatbot.benchmark.benchmark_test import BenchmarkTest
from ezchatbot.benchmark.matchers import (
    ContainsAllResponsesMatcher,
    ContainsResponsesMatcher,
    EndsWithResponseMatcher,
    ExactResponseMatcher,
    FunctionCallingMatcher,
    MatcherParams,
    NotContainsResponseMatcher,
    RegexResponseMatcher,
    ResponseMatcher,
    StartsWithResponseMatcher,
)
from ezchatbot.benchmark.results import BenchmarkResults
from ezchatbot.chatbots.interface import IChatbot
from ezchatbot.commands import CommandParameters, CommandsContainer


@dataclass
class Benchmark:
    """
    Benchmarking tool for your chatbot, it helps you finding the best models and parameters to
    handle your queries.
    """

    chatbot_class: Type[IChatbot]
    parameters: dict[str, object]

    def __post_init__(self):
        self._results = BenchmarkResults()
        labels = [
            *self.parameters.keys(),
        ]
        combinations = itertools.product(
            *[
                list(params)
                for params in self.parameters.values()
                if isinstance(params, Iterable)
            ]
        )
        self._parameters_combinations: list[dict[str, object]] = [
            dict(zip(labels, comb)) for comb in combinations
        ]
        self._tests: list[BenchmarkTest] = []
        self._commands = CommandsContainer.empty()

    def add_test(  # pylint: disable=too-many-arguments
        self,
        query: str,
        function_calling: Optional[list[CommandParameters]] = None,
        answer: Optional[str] = None,
        exact_answer: Optional[str] = None,
        starts_with_answer: Optional[str] = None,
        ends_with_answer: Optional[str] = None,
        contained_in_answer: Optional[list[str]] = None,
        all_in_answer: Optional[list[str]] = None,
        not_in_answer: Optional[list[str]] = None,
        regex_answer: Optional[str] = None,
        language_answer: Optional[str] = None,
        **kwargs: Unpack[MatcherParams],
    ):
        """
        Add a test to the Benchmark suite. Only a query and one of the other parameters should be set.
        Each parameter corresponds to a different kind of test to perform.

        Args:
            query (str): The query to test
            function_calling (Optional[str]): The function call expected (like: `say_hello(name="Ben")`)
            answer (Optional[str]): The expected answer, the result will be compared by an LLM to know if it corresponds
                to the expected answer.
            exact_answer (Optional[str]): The expected answer. It should be exactly the same.
            starts_with_answer (Optional[str]): The start of the expected answer.
            ends_with_answer (Optional[str]): The end of the expected answer.
            contained_in_answer (Optional[list[str]]): A list of strings, at least one of them should be in the result.
            all_in_answer (Optional[list[str]]): A list of strings, all of them should be in the result.
            not_in_answer (Optional[list[str]]): A list of strings, none of them should be in the result.
            regex_answer (Optional[str]): A regex which should match the result.
            language_answer (Optional[str]): The language of the answer, it will be evaluated by an LLM.
        """
        # TODO: add custom matchers
        # TODO: rename "answer" to "ollama_answer" and language_answer to ollama_language
        # TODO: add a test for function response
        if not query:
            raise ValueError("Query should be set")

        non_empty_params = [
            param
            for param in [
                function_calling,
                answer,
                exact_answer,
                starts_with_answer,
                ends_with_answer,
                contained_in_answer,
                all_in_answer,
                not_in_answer,
                regex_answer,
                language_answer,
            ]
            if param
        ]

        if len(non_empty_params) != 1:
            raise ValueError("Only one parameter (other than the query) can be set")

        # TODO: handle answer and language_answer
        matcher: Optional[ResponseMatcher] = None
        if function_calling:
            matcher = FunctionCallingMatcher(function_calling)
        # if answer:
        #     matcher = FunctionCallingMatcher([answer])
        if exact_answer:
            matcher = ExactResponseMatcher(exact_answer)
        if starts_with_answer:
            matcher = StartsWithResponseMatcher(starts_with_answer)
        if ends_with_answer:
            matcher = EndsWithResponseMatcher(ends_with_answer)
        if contained_in_answer:
            matcher = ContainsResponsesMatcher(contained_in_answer)
        if all_in_answer:
            matcher = ContainsAllResponsesMatcher(all_in_answer)
        if not_in_answer:
            matcher = NotContainsResponseMatcher(not_in_answer)
        if regex_answer:
            matcher = RegexResponseMatcher(regex_answer)
        # if language_answer:
        #     matcher = FunctionCallingMatcher([language_answer])

        if matcher is None:
            raise ValueError("No matcher was set")

        matcher.load_params(kwargs)

        self._tests.append(
            BenchmarkTest(
                self.chatbot_class,
                self._parameters_combinations,
                self._commands,
                query,
                matcher,
            )
        )

    def command(self):
        """
        A decorator to define a command in the benchmark
        """

        def inner(func: Callable):
            self._commands.add_command(func)

        return inner

    def run(self) -> BenchmarkResults:
        """
        Run the benchmark and return the results.

        Returns:
            BenchmarkResults: The results of the benchmark
        """
        for i, test in enumerate(self._tests):
            print(f"Running test {i + 1}/{len(self._tests)}: {test.query}")
            cur_results = test.run()
            for result in cur_results:
                self._results.add_result(result)
        self._results.compile()
        return self._results

    def get_results(self) -> BenchmarkResults:
        """
        Get the results of the benchmark.

        Returns:
            BenchmarkResults: The results of the benchmark
        """
        return self._results
