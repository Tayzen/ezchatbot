"""
Results for the benchmark
"""

import json
from dataclasses import dataclass, field
from typing import Optional

import pandas as pd

from ezchatbot.benchmark.benchmark_test import UnitResult


@dataclass
class ConfigWrapper:
    """
    A config for the benchmark
    """

    config: dict[str, object]

    def save(self, path: str):
        """
        Save the config as a json file
        """
        with open(path, "w", encoding="utf-8") as fp:
            json.dump(self.config, fp)

    def __str__(self):
        return json.dumps(self.config)


@dataclass
class CompiledResult:
    """
    The result of the benchmark for a specific config
    """

    config: ConfigWrapper
    accuracy: float
    avg_duration_ns: float

    def __str__(self):
        return (
            f"CompiledResult({str(self.config)}, accuracy={self.accuracy:.2f}"
            + f", avg_duration_ms={int(self.avg_duration_ns // 1_000_000)})"
        )


class DataNotCompiledError(Exception):
    """
    The data has not been compiled
    """


@dataclass
class BenchmarkResults:
    """
    Handles the results of the benchmark
    """

    unit_results: list[UnitResult] = field(default_factory=list)
    raw: Optional[pd.DataFrame] = None
    compiled_results: list[CompiledResult] = field(default_factory=list)

    def add_result(self, result: UnitResult):
        """
        Add a result from the benchmark

        Args:
            result (UnitResult): The result to add
        """
        self.unit_results.append(result)

    def compile(self) -> list[CompiledResult]:
        """
        Compile the results

        Returns:
            list[CompiledResult]: The compiled results
        """
        tmp_list = []
        encountered_configs: list[ConfigWrapper] = []
        for result in self.unit_results:
            config = ConfigWrapper(result.config)
            if config in encountered_configs:
                config_index = encountered_configs.index(config)
            else:
                config_index = len(encountered_configs)
                encountered_configs.append(config)

            tmp_list.append(
                (
                    config_index,
                    *result.config.values(),
                    result.query,
                    result.response,
                    int(result.correctness),
                    result.duration_ns,
                )
            )
        headers = [
            "config_index",
            *self.unit_results[0].config.keys(),
            "query",
            "response",
            "correctness",
            "duration_ns",
        ]
        self.raw = pd.DataFrame(tmp_list, columns=headers)

        duration_df = self.raw.groupby("config_index")["duration_ns"].mean()
        accuracy_df = self.raw.groupby("config_index")["correctness"].mean()
        self.compiled_results = [
            CompiledResult(
                config=ConfigWrapper(config.config),
                accuracy=float(accuracy_df[config_index]),
                avg_duration_ns=float(duration_df[config_index]),
            )
            for config_index, config in enumerate(encountered_configs)
        ]

        return self.compiled_results

    def get_best_accuracy(self) -> ConfigWrapper:
        """
        Get the best result for a specific metric

        Returns:
            ConfigWrapper: The best configuration
        """
        if len(self.compiled_results) == 0:
            self.compile()

        best = self.compiled_results[0]
        for result in self.compiled_results[1:]:
            if result.accuracy > best.accuracy:
                best = result

        return best.config

    def get_best_duration(self) -> ConfigWrapper:
        """
        Get the best result for a specific metric

        Returns:
            ConfigWrapper: The best configuration
        """
        if len(self.compiled_results) == 0:
            self.compile()

        best = self.compiled_results[0]
        for result in self.compiled_results[1:]:
            if result.avg_duration_ns < best.avg_duration_ns:
                best = result

        return best.config

    def get_config(self, index: int) -> ConfigWrapper:
        """
        Get the config at a specific index

        Args:
            index (int): The index of the config

        Returns:
            ConfigWrapper: The configuration
        """
        return self.compiled_results[index].config

    def to_csv(self, path: str):
        """
        Save the results to a csv file
        """
        if self.raw is not None:
            self.raw.to_csv(path)
        else:
            raise DataNotCompiledError("You must run the `compile()` method before")

    def to_json(self, path: str):
        """
        Save the results to a json file
        """
        if self.raw is not None:
            self.raw.to_json(path)
        else:
            raise DataNotCompiledError("You must run the `compile()` method before")

    def to_html(self, path: str):
        """
        Save the results to a html file
        """
        if self.raw is not None:
            self.raw.to_html(path)
        else:
            raise DataNotCompiledError("You must run the `compile()` method before")

    def to_md(self, path: str):
        """
        Save the results to a markdown file
        """
        if self.raw is not None:
            self.raw.to_markdown(path)
        else:
            raise DataNotCompiledError("You must run the `compile()` method before")

    def to_latex(self, path: str):
        """
        Save the results to a latex file
        """
        if self.raw is not None:
            self.raw.to_latex(path)
        else:
            raise DataNotCompiledError("You must run the `compile()` method before")

    def show(self, n):
        """
        Show the results
        """
        if self.compiled_results:
            best_results = [
                f"{self.compiled_results.index(result)}: {result}"
                for result in sorted(
                    self.compiled_results, key=lambda x: x.accuracy, reverse=True
                )[0:n]
            ]
            inner_str = "\n  " + "\n  ".join(best_results)
            if len(self.compiled_results) > n:
                inner_str += "  ..."
            inner_str += "\n"
            return f"BenchmarkResults({inner_str})"
        if self.raw is not None:
            return f"BenchmarkResults({self.raw.shape[0]} raw results, not compiled)"
        return "BenchmarkResults(empty)"

    def __str__(self) -> str:
        return self.show(5)
