import time
from dataclasses import dataclass, field
from typing import Type

from tqdm.auto import tqdm

from ezchatbot.benchmark.matchers import FunctionCallingMatcher, ResponseMatcher
from ezchatbot.chatbots.interface import IChatbot
from ezchatbot.chatbots.messages import NO_RESPONSE, NoResponse
from ezchatbot.commands import CommandParameters, CommandsContainer


@dataclass
class UnitResult:
    """
    The result of a test for a specific config
    """

    query: str
    config: dict[str, object]
    response: str
    correctness: bool
    duration_ns: int  # TODO: duration in ms


def _format_result(result: str | list[CommandParameters] | NoResponse) -> str:
    if isinstance(result, list):
        return "\n".join(str(cmd) for cmd in result)
    if isinstance(result, NoResponse):
        return "No response"
    return result


@dataclass
class BenchmarkTest:
    """
    A test for an unique query over multiple configs
    """

    chatbot_class: Type[IChatbot]
    configs: list[dict[str, object]]
    commands_container: CommandsContainer
    query: str
    matcher: ResponseMatcher
    results: list[UnitResult] = field(default_factory=list)

    def __post_init__(self):
        if isinstance(self.matcher, FunctionCallingMatcher):
            self.matcher.add_commands_container(self.commands_container)

    def run(self) -> list[UnitResult]:
        """
        Run the test

        Returns:
            list[UnitResult]: The results of the test
        """
        # TODO: find a way to pass an argument to unload the ollama model while staying general
        for config in tqdm(self.configs):
            tmp_class = self.chatbot_class(**config, _commands=self.commands_container)  # type: ignore

            start_fc = time.perf_counter_ns()
            result_fc = tmp_class.execute_function_calling_model(self.query)
            result: str | list[CommandParameters] | NoResponse = result_fc
            end_fc = time.perf_counter_ns()
            duration_fc_ns = end_fc - start_fc

            duration_backup = 0
            if result_fc == NO_RESPONSE or not self.commands_container.is_valid(result_fc):  # type: ignore
                start_backup = time.perf_counter_ns()
                result_backup = tmp_class.execute_backup_model(self.query)
                result = result_backup
                end_backup = time.perf_counter_ns()
                duration_backup = end_backup - start_backup

            duration_ns = duration_fc_ns + duration_backup

            # TODO: store the results somewhere (in case the computation failed)
            self.results.append(
                UnitResult(
                    query=self.query,
                    config=config,
                    response=_format_result(result),
                    correctness=(
                        self.matcher.match(result)
                        if not isinstance(result, NoResponse)
                        else False
                    ),
                    duration_ns=duration_ns,
                )
            )
        return self.results
