import os

from ezchatbot import Benchmark, OllamaChatbot

benchmark = Benchmark(
    OllamaChatbot,
    parameters={
        "url": ["localhost"],
        "port": [11434],
        "function_callling_model": [
            "llama3.1:8b",
            "gemma2:2b",
            "gemma2:9b",
        ],
        "function_calling_temperature": [0, 0.2, 0.5, 1],
        "backup_model": ["llama3.1:8b", "gemma2:9b"],
    },
)


@benchmark.command()
def hello(name: str) -> str:
    """
    Say hello to the user with his name.
    """
    return f"Salut {name}!"


@benchmark.command()
def get_weather(city: str) -> str:
    """
    Get the weather for the given city.
    """
    return f"À {city} le temps est clair."


@benchmark.command()
def get_room_temperature(room_name: str) -> str:
    """
    Get the temperature for the given room of the house (in celcius).
    """
    return f"La température de la pièce : {room_name} est de 20 degrés celcius."


benchmark.add_test(
    query="Quel temps fait-il à Paris ?",
    function_calling=["get_weather(city='Paris')"],
)
benchmark.add_test(
    query="What is the height of the Eiffel tower? (only respond with the number in meters)",
    exact_answer="330",
)
benchmark.add_test(
    query="Give me a synonym of 'sunny'",
    contained_in_answer=["bright", "clear", "luminous"],
    case_sensitive=False,
)

results = benchmark.run()
print(results)

output_folder = os.path.join(os.path.dirname(__file__), "benchmark_output")

results.to_md(os.path.join(output_folder, "results.md"))

best = results.get_best_accuracy()
print(best)

best.save(os.path.join(output_folder, "best.json"))
