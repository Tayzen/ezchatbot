from ezchatbot import OllamaChatbot

chatbot = OllamaChatbot(
    url="localhost",
    port=11434,
    function_callling_model="llama3.1:8b",
    function_calling_temperature=0,
    backup_model="llama3.1:8b",
)


# The following functions are just placeholders, you can do whatever you want with them
@chatbot.command()
def hello(name: str) -> str:
    """
    Say hello to the user with his name.
    """
    return f"Hi {name}!"


@chatbot.command()
def get_weather(city: str) -> str:
    """
    Get the weather for the given city.
    """
    return f"In {city} the weather is sunny, the temperature is 25 degrees celcius."


@chatbot.command()
def get_room_temperature(room_name: str) -> str:
    """
    Get the temperature for the given room of the house (in celcius).
    """
    return f"The temperature of the {room_name} is 20 degrees celcius."


# Queries using function calling:
print(chatbot.execute_query("Hi, I'm Benjamin"))
# Hi Benjamin!

print(chatbot.execute_query("What's the weather in Paris ?"))
# The weather in Paris is sunny, the temperature is 25 degrees celcius.

print(chatbot.execute_query("What's the temperature in the living room ?"))
# The temperature of the living room is 20 degrees celcius.

# Query with no matching function:
print(chatbot.execute_query("What's the size of the Eiffel Tower?"))
# The size of the Eiffel Tower is 330 meters.
