from ezchatbot import OllamaChatbot, ServerHandler

# You can use the configuration of your choice exported from the benchmark (or create your json manually of you prefer)
chatbot = OllamaChatbot.from_config("benchmark_output/best.json")


# The following functions are just placeholders, you can do whatever you want with them
@chatbot.command()
def hello(name: str) -> str:
    """
    Say hello to the user with his name.
    """
    return f"Hi {name}!"


@chatbot.command()
def get_weather(city: str) -> str:
    """
    Get the weather for the given city.
    """
    return f"In {city} the weather is sunny, the temperature is 25 degrees celcius."


@chatbot.command()
def get_room_temperature(room_name: str) -> str:
    """
    Get the temperature for the given room of the house (in celcius).
    """
    return f"The temperature of the {room_name} is 20 degrees celcius."


# Serve the chatbot using fastAPI, read the doc to see the endpoints
app = ServerHandler(chatbot).serve()
