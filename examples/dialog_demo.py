from ezchatbot import OllamaChatbot

# You can use the configuration of your choice exported from the benchmark (or create your json manually of you prefer)
chatbot = OllamaChatbot.from_config("best.json")


# The following functions are just placeholders, you can do whatever you want with them
@chatbot.command()
def hello(name: str) -> str:
    """
    Say hello to the user with his name.
    """
    return f"Hi {name}!"


@chatbot.command()
def get_weather(city: str) -> str:
    """
    Get the weather for the given city.
    """
    return f"In {city} the weather is sunny, the temperature is 25 degrees celcius."


@chatbot.command()
def get_room_temperature(room_name: str) -> str:
    """
    Get the temperature for the given room of the house (in celcius).
    """
    return f"The temperature of the {room_name} is 20 degrees celcius."


# The dialog feature could be awesome if you want to make an app running without a server/client architecture.
# It's an handy object which keeps track of the conversation between the user and the bot, it manages it for you.
dialog = chatbot.start_dialog()
print(dialog.speak("What's the size of the Eiffel Tower?"))
# The size of the Eiffel Tower is 330 meters
print(dialog.speak("Where is it located?"))
# The Eiffel Tower is located in Paris, France
print(dialog.speak("What's the capital of the country?"))
# The capital of the country is Paris
