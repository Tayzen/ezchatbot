# EZ-Chatbot

A Python library to make the creation of a chatbot easy, thanks to the LLMs and ollama (more engines coming).

## Why and how?

EZChatbot enables you to create a chatbot with custom capabilities in seconds. It's the perfect solution if you want to:

- add a chatbot ability to your existing app
- create your own to make your life easier
- build a simple service to help your colleagues to interact with the documents of the company
- whatever you want

Creating a chatbot was really hard. Computers are not made for understanding natural language and do it programmatically is almost impossible.

Quite recently OpenAI released ChatGPT and shows to the world that Large Language Models are capable of processing human languages with astonishing results. This is really great, but you can't ask an LLM to execute your business logic and for a lot of tasks where you want an exact result they aren't even reliable.

The best solution should be to ask an LLM to translate your natural language into something you can parse and easily translate in a code execution. Well, this is exactly what this library is all about.

This method of converting natural language to a parsable item (JSON) is called **function calling** (or sometimes **tool calling**). This technics is becoming a standard, and you can use it with almost every LLM available (ChatGPT, Ollama, Claude, etc.). So, that's not with this functionality that the library is shining. The main interest is the development experience, EZChatbot is made to be really easy to use and flexible for complex needs. For example, with EZChatbot, you don't need to:

- writing manually all the json schemas corresponding to your functions (moreover, as it is an automatic process, the maintenance is not needed)
- converting the function calls from the llm to code execution
- handling the errors
- handling the false positive of the function calling (when the LLM returns a function that doesn't exist or bad parameters)
- manually keeping track of the messages in a conversation
- write your APIs to deploy the Chatbot
- testing manually what is the optimal combination of parameters (temperature, model, top_p, top_k, system prompt, etc.) to use.

For example, this is how to declare a chatbot using Ollama with some custom functions, executes some queries and serving APIs to interact with it:

```python
from ezchatbot import OllamaChatbot, ServerHandler

chatbot = OllamaChatbot(url="localhost", port=51860)

@chatbot.command()
def get_weather(city: str, unit: Literal["C", "F"]) -> str:
    """
    Get the weather informations for a city with temperature in the requested unit (Celcius: C or Farenheit: F)
    """
    # Do an API call and parsing, this is just a mockup
    temperature, humidity, sky_condition = 22, 30, "cloudy"
    return f"The temperature in {city} is {temperature}°{unit}, the sky is {sky_condition} and the humidity is {humidity}%"


@benchmark.command()
def get_room_temperature(room_name: str) -> str:
    """
    Get the temperature for the given room of the house (in celcius).
    """
    return f"The temperature in the room : {room_name} is 20°C."

chatbot.execute_query("What is the weather in Paris?")
# The temperature in Paris is 22°C, the sky is cloudy and the humidity is 30%

# The models are often multilingual, here it also work with French.
# The reponse will not automatically as of today but the feature is coming
chatbot.execute_query("Quel temps fait-il à Paris ?")
# The temperature in Paris is 22°C, the sky is cloudy and the humidity is 30%

# Your functions cannot perform every action but you can have a backup response in case a user query is not supported (this can be disabled and replace by a sentence of your choice if you prefer)
chatbot.execute_query("What is the height of the Eiffel tower?")
# "The height of the Eiffel tower is approximately 330 meters from its base to the tip of the antenna."

app = ServerHandler(chatbot).serve()
```

For more examples, you can consult the `examples` folder.

## Installation

**This is not yet available**

```bash
pip install --user ezchatbot
```

## Technical details

As you can see of the flowchart, the behaviour of EZChatbot is quite simple. It can be split into 3 parts:

1. The tool calling: The LLM which was chosen for this function will return a JSON representing the function call to make to respond to the query of the user.
1. Handling the response: EZChatbot will handle errors (if any) and check for false positive. If there is any issue, the library will launch another generation using the backup model. Otherwise, it executes the function and return the result. A new feature will come to add another step of generation to reformulate the response from your function before returning the result (for translation or rephrasing).
1. If the function calling wasn't successful, another model will be called to generate an answer to the user's query. This step could be disabled if you want to control the experience for your users.

```mermaid
flowchart
    A[User] -->|Send a query| B(EZChatbot)
    B --> C{Function Calling}
    C -->|Response + No errors| D{Parsing + Execution}
    D -->|Code execution| F[Formating the response]
    F -->|Send response| A
    D -->|Error during parsing or execution| E{Backup Model}
    C -->|Error| E
    C -->|No Response| E
    E -->|Error| G[Sorry message]
    E -->|No error, sending response| A
    G -->|Sending response| A
```

## Coming soon (or not)

EZChatbot is a temporary name, so the first thing coming soon is a rebranding!

Hopefully, that's not the only change, some features are also coming in the next months:

- Adding the support for the new ollama built-in tool calling
- LLMs matchers for the benchmark
- Fixing an issue with the benchmark of ollama which makes the duration unreliable
- More options for the command decorator
- The support for new engines (at least ChatGPT, probably Claude)
- A new handler to build quickly your RAG with a document retrieval based on ChromaDB
- Streaming
- A logging system

Others are wanted but judged not as important:

- Multiple choices of responses
- Support for images?
- Option to automatically fill a sqlite database with requests?

## Inspirations

- [ChatGPT's function calling feature](https://platform.openai.com/docs/guides/function-calling) the first feature build on top of an LLM which really blows my mind
- [Ollama](https://ollama.com) it interests myself to the subject of LLMs because of the local first philosophy, I saw a great potential to build tools on top of it
- [Tiangolo for the syntax (the developer behind the fantastic FastAPI and Typer)](https://fastapi.tiangolo.com/history-design-future/#design)
